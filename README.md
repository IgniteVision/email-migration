# Email Migration

## Requirements
- PHP 7+
- http://php.net/imap

## Setup
```php
<?php
$config = [
	'src' => [
		'user' => 'src.email@example.com',
		'pass' => 'password',
		'connection' => '{example.com:995/pop3/ssl}',
		'separator' => '/',
		'startFrom' => 1
	],
	'dst' => [
		'user' => 'dst.email@example.com',
		'pass' => 'password',
		'connection' => '{example.com:993/imap/ssl/novalidate-cert}',
		'separator' => '.',
		'inFolder' => 'INBOX' // The parameter is required when the source is pop3
	]
];

$emailMigration = new EmailMigration($config);
?>
```
