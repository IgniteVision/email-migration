<?php

class EmailMigration {

	private $config = [];
	private $srcBox = null;
	private $dstBox = null;
	private $fetchOverviewLimit = 100;

	public function __construct(array $config) {
		$this->config = $this->verifyConfig($config);
		$this->run();
	}

	private function verifyConfig($config) {
		$example = [
			'src' => [
				'user' => null,
				'pass' => null,
				'connection' => null,
				'separator' => null
			],
			'dst' => [
				'user' => null,
				'pass' => null,
				'connection' => null,
				'separator' => null
			]
		];

		if(!empty($diff = array_diff_key($example, $config)))
			throw new Exception('Please check your config. Missing params: '.implode(', ', array_keys($diff)));

		if(!empty($diff = array_diff_key($example['src'], $config['src'])))
			throw new Exception('Please check your config in section "src". Missing params: '.implode(', ', array_keys($diff)));

		if(!empty($diff = array_diff_key($example['dst'], $config['dst'])))
			throw new Exception('Please check your config in section "dst". Missing params: '.implode(', ', array_keys($diff)));

		return $config;
	}

	private function run() {
		if(!array_key_exists('startFrom', $this->config['src']))
			$this->config['src']['startFrom'] = 1;

		$this->srcBox = $this->open('src');
		$this->dstBox = $this->open('dst');

		$inFolder = (!isset($this->config['dst']['inFolder']) || empty($this->config['dst']['inFolder'])) ? null : $this->config['dst']['inFolder'];

		if(stripos($this->config['src']['connection'], '/pop3') !== false) {
			$this->pop3($inFolder);
		} else {
			$this->imap($inFolder);
		}

		imap_close($this->srcBox);
		imap_close($this->dstBox);
	}

	private function pop3($inFolder) {
		if(empty($inFolder))
			throw new Exception('POP3 requires "inFolder" in "dst"');

		$emails = imap_num_msg($this->srcBox);
		for($i = $this->config['src']['startFrom']; $i <= $emails; $i++) {
			if(!imap_ping($this->srcBox))
				$this->srcBox = $this->open('src', $srcFolder);

			$message = imap_fetchheader($this->srcBox, $i) . imap_body($this->srcBox, $i);
			if(empty($message))
				throw new Exception('Cannot fetch email #'.$i.' ('.imap_last_error().')');

			if(!imap_ping($this->dstBox)) {
				if(!imap_reopen($this->dstBox, $this->config['dst']['connection'].$dstFolder))
					throw new Exception('Cannot open '.$this->config['dst']['connection'].$dstFolder.' ('.imap_last_error().')');
			}

			if(!imap_append($this->dstBox, $this->config['dst']['connection'].$inFolder, $message, '\Seen'))
				throw new Exception('Cannot append email #'.$i.' ('.imap_last_error().')');
		}
	}

	private function imap($inFolder) {
		if(!empty($inFolder))
			$inFolder.= $this->config['dst']['separator'];

		$folders = [];
		$srcFolders = $this->getFolders('src', $this->config['src']['connection']);
		foreach($srcFolders as $folder) {
			$dstFolder = $folder;
			if($this->config['src']['separator'] != $this->config['dst']['separator'])
				$dstFolder = str_replace($this->config['dst']['separator'], '-', $dstFolder);

			$_inFolder = $folder == $this->config['dst']['inFolder'] ? null : $inFolder;
			$folders[$folder] = $_inFolder.str_replace($this->config['src']['separator'], $this->config['dst']['separator'], $dstFolder);
		}

		foreach($folders as $srcFolder => $dstFolder) {
			$dir = explode($this->config['dst']['separator'], $dstFolder);
			array_pop($dir);
			$rootFolder = implode($this->config['dst']['separator'], $dir);
			if(!imap_reopen($this->dstBox, $this->config['dst']['connection'].$rootFolder))
				throw new Exception('Cannot open '.$this->config['dst']['connection'].$rootFolder.' ('.imap_last_error().')');

			$this->createFolder($this->config['dst']['connection'].$dstFolder);

			$this->srcBox = $this->open('src', $srcFolder); // imap_reopen: Couldn't re-open stream
			if(!imap_reopen($this->dstBox, $this->config['dst']['connection'].$dstFolder))
				throw new Exception('Cannot open '.$this->config['dst']['connection'].$dstFolder.' ('.imap_last_error().')');

			$srcBoxCheck = imap_check($this->srcBox);
			$dstBoxCheck = imap_check($this->dstBox);

			$emailsLeft = $srcBoxCheck->Nmsgs % $this->fetchOverviewLimit;
			$fetchOverviewLoops = ($srcBoxCheck->Nmsgs - $emailsLeft) / $this->fetchOverviewLimit;
			for($i = 1; $i <= $fetchOverviewLoops; $i++) {
				$startEmail = $i == 1 ? $i : ($i * $this->fetchOverviewLimit - $this->fetchOverviewLimit);
				$endEmail = $i >= $fetchOverviewLoops ? ($startEmail + $this->fetchOverviewLimit + $emailsLeft) : ($startEmail + $this->fetchOverviewLimit - ($i == 1 ? 2 : 1));
				if($startEmail < $this->config['src']['startFrom'])
					continue;

				$srcMails = imap_fetch_overview($this->srcBox, $startEmail.':'.$endEmail, 0);
				$dstMails = imap_fetch_overview($this->dstBox, $startEmail.':'.$endEmail, 0);

				foreach($srcMails as $mail) {
					if(property_exists($mail, 'message_id')) {
						foreach($dstMails as $dstMail) {
							if(property_exists($dstMail, 'message_id')) {
								if(trim($mail->message_id) == trim($dstMail->message_id))
									continue 2;
							}
						}
					}

					if(!imap_ping($this->srcBox))
						$this->srcBox = $this->open('src', $srcFolder);

					if(empty($message = imap_fetchheader($this->srcBox, $mail->msgno) . imap_body($this->srcBox, $mail->msgno)));
						throw new Exception('Cannot fetch email #'.$mail->msgno.' ('.imap_last_error().')');

					if(!imap_ping($this->dstBox)) {
						if(!imap_reopen($this->dstBox, $this->config['dst']['connection'].$dstFolder))
							throw new Exception('Cannot open '.$this->config['dst']['connection'].$dstFolder.' ('.imap_last_error().')');
					}

					if(!imap_append($this->dstBox, $this->config['dst']['connection'].$dstFolder, $message, $mail->seen == 1 ? '\Seen' : null))
						throw new Exception('Cannot append email #'.$mail->msgno.' ('.imap_last_error().')');
				}
			}
		}
	}

	private function open($box, $dir = null) {
		if(!empty($this->{$box.'Box'}))
			imap_close($this->{$box.'Box'});

		if(!($imap = imap_open($this->config[$box]['connection'].$dir, $this->config[$box]['user'], $this->config[$box]['pass'])))
			throw new Exception(imap_last_error());

		return $imap;
	}

	private function getFolders($box, $string) {
		if(!($folders = imap_list($this->{$box.'Box'}, $string, '*')))
			throw new Exception('Can\'t get folder info: '. imap_last_error());

		return array_map(function($value) use ($string) { return str_replace($string, '', $value); }, $folders);
	}

	private function createFolder($path) {
		$encodedPath = imap_utf7_encode($path);
		$folders = $this->getFolders('dst', $this->config['dst']['connection']);
		if(!in_array(str_replace($this->config['dst']['connection'], '', $encodedPath), $folders)) {
			if(!imap_createmailbox($this->dstBox, $encodedPath))
				throw new Exception('Error creating folder: '.$path.' ('.imap_last_error().')');
		}
	}

}
?>
